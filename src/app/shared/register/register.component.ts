import {Component, OnInit, Output, EventEmitter, Input, ChangeDetectionStrategy} from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Store } from '@ngrx/store';
import {User} from 'src/app/core/users.store';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RegisterComponent implements OnInit {
  registerForm = new FormGroup({
    id: new FormControl(0),
    email: new FormControl(''),
    password: new FormControl(''),
    avatar: new FormControl('')
  });
  id = null;
  @Output() onSubmit = new EventEmitter<User>();
  @Input() btnName: string;
  @Input()
  set user(user: User) {
     if(user.id == null) { user.id  = 0}
    const initValue = {...this.registerForm.value}
    Object.keys(initValue).forEach(key => {
      initValue[key] = user[key];
    });
    this.registerForm.setValue(initValue);
  }
  ngOnInit(): void {
  }
}
