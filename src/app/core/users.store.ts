import { createAction, createReducer, props, Action, on } from '@ngrx/store';

export const addUser = createAction(
  'addUser',
  props<{user: User}>()
);

export const updateUser = createAction(
  'updateUser',
  props<{user: Partial<User>, id: number}>()
);

export const deleteUser = createAction(
  'deleteUser',
  props<{id: number}>()
);

export interface User {
  id: number,
  email: string,
  password: string,
  avatar: string
}

export interface UsersState {
  nextId: number;
  users: User[];

}

const initialState: UsersState = {
  nextId: 0,
  users: [],
}

const scoreboardReducer = createReducer(
  initialState,
  on(addUser, (state, {user}) => ({users: [...state.users, {...user, id: state.nextId}], nextId: state.nextId+1})),
  on(updateUser, (state, {user, id}) => {
    console.log(user);

    const users = [...state.users];
    const index = users.findIndex(x => x.id === id);
    users[index] = {...users[index], ...user};
    console.log(users);
    return {...state, users}
  }),
  on(deleteUser, (state, {id}) => {
    const users = [...state.users]
    const index = users.findIndex(x => x.id === id);
    users.splice(index, 1)
    return {...state, users}
  })


);

export function userReducer(state: UsersState | undefined, action: Action) {
  return scoreboardReducer(state, action);
}
