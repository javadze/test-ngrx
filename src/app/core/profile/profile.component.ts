import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthStateService } from '../auth-state.service';
import { User } from '../users.store';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
 user: Observable<User>;
  constructor(private authSS: AuthStateService) {
    this.user = authSS.user.asObservable();
   }

  ngOnInit(): void {
  }

  setProfile(user) {
    console.log(user);

    this.authSS.onRegister(user);
  }

}
