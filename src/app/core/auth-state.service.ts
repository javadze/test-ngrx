import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import {not} from 'rxjs/internal-compatibility';
import { User } from './users.store';

@Injectable({
  providedIn: 'root'
})
export class AuthStateService {
  isAuthorized = new BehaviorSubject(false);
   user: BehaviorSubject<User>= new BehaviorSubject(null);
  constructor(private router: Router) { }

  changeAuthSate(isAuthorized: boolean): void {
    this.isAuthorized.next(isAuthorized);
  }

  onRegister(user): void {
    this.user.next(user);
  }

  onLogin(user): void {
    const notIncludeFalseValue = !Object.keys(user).map(key => {
     return  this.user.value[key] ===  user[key];
    }).includes(false);
    this.isAuthorized.next(notIncludeFalseValue);
    console.log(this.isAuthorized.value);
    if (notIncludeFalseValue) {
      this.router.navigate(['/search']);
    }
  }

}
