import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Store } from '@ngrx/store';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { addUser, deleteUser, updateUser, User, UsersState } from 'src/app/core/users.store';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
  user = new BehaviorSubject({
    id: 0,
    email: '',
    password: '',
    avatar: ''
  })
  users: Observable<User[]>;
  isEdit = false
  constructor(private store: Store<any>) {
    this.users = store.select('users').pipe(map(userState => userState.users));
   }

  ngOnInit(): void {
  }


  initUser () {
    this.user.next({
      id: 0,
      email: '',
      password: '',
      avatar: ''
    })
  }


  addUser (user) {
    this.store.dispatch(addUser({user}))
    this.initUser()

  }

  updateUser(user) {
    this.isEdit =false;
    this.store.dispatch(updateUser({user, id: user.id}))
    this.initUser()

  }

  deleteUser(id: number) {
    this.store.dispatch(deleteUser({id}))

  }
  onReadyToEdit(user) {
    this.isEdit = true;
    const initValue = {...this.user.value};
    Object.keys(initValue).forEach(key => {
      initValue[key] = user[key];
    });
    this.user.next(initValue);
  }

  submit(user) {
    this.isEdit ? this.updateUser(user) : this.addUser(user)
  }
}
