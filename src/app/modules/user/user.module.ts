import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserComponent } from './user.component';
import {RouterModule} from '@angular/router';
import { StoreModule } from '@ngrx/store';
import { userReducer } from 'src/app/core/users.store';
import { SharedModule } from 'src/app/shared/shared.module';
import { ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [UserComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: UserComponent
      }
    ]),
    StoreModule.forFeature('users',userReducer),
    SharedModule,
    ReactiveFormsModule
  ]
})
export class UserModule { }
