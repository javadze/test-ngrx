import { Component, OnInit } from '@angular/core';
import {FormControl} from '@angular/forms';
import {SearchService} from './search.service';
import {SearchApiModel} from './search.models';
import { catchError, debounceTime, distinctUntilChanged, map, switchMap } from 'rxjs/operators';
import { of } from 'rxjs';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  searchForm = new FormControl('');
  searchValue: SearchApiModel.Datum[] = [];
  constructor(private searchService: SearchService) { }

  ngOnInit(): void {
    this.searchForm.valueChanges
    .pipe(
      distinctUntilChanged(),
      debounceTime(1000),
      switchMap((search) => this.searchService.search(search)),
      map(searchRes => searchRes.data),
      catchError(err => of(null))
    ).subscribe(searchValues => {
       this.searchValue = [...searchValues];
    });
  }




}
