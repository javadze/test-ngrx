import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {SearchApiModel} from './search.models';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SearchService {

  constructor(private http: HttpClient) { }

    search(query: string): Observable<SearchApiModel.RootObject>  {
    return  this.http.get<SearchApiModel.RootObject >(environment.apiBaseUrl + '/gifs/search', {
      params: {
        api_key: environment.apiKey,
        q: query
      }
    });
  }
}
