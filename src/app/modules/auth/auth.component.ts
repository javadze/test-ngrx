import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import {User} from '../../core/users.store';
import {AuthStateService} from '../../core/auth-state.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {
  loginForm =new FormGroup({
    email: new FormControl(''),
    password: new FormControl('')
  });
  constructor(private authSS: AuthStateService) { }

  ngOnInit(): void {
  }

  onLogin(): void {
    this.authSS.onLogin(this.loginForm.value);
  }

  onRegister(user: User): void {
    this.authSS.onRegister(user);
  }

}
