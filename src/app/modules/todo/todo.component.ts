import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { TodoDialogComponent } from './todo-dialog/todo-dialog.component';
import {ITodo} from './todo.model';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.scss']
})
export class TodoComponent implements OnInit {
  todos: ITodo[] = [];
  constructor(private dialog: MatDialog) {}
  ngOnInit(): void {
  }


  openDialog(todo?: ITodo, i?: number): void {
    const index = i;
    const dialogRef = this.dialog.open(TodoDialogComponent, {
      width: '250px',
      data: todo
    });

    dialogRef.afterClosed().subscribe(result => {
      if (this.todos.length !== 0) {
         this.todos[index] = result
        } else {

          this.todos.push(result)
        }
    });
  }

  onDone(i: number) {
    const todos = this.todos
    todos[i].isDone = !todos[i].isDone
    console.log(todos[i]);

    this.todos = [...todos];
  }

}
