export interface ITodo {
   text: string
   isDone: boolean
   description: string
}
