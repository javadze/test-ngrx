import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TodoComponent } from './todo.component';
import {RouterModule} from '@angular/router';
import { TodoDialogComponent } from './todo-dialog/todo-dialog.component';
import {MatDialogModule} from '@angular/material/dialog';
import { ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [TodoComponent, TodoDialogComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: TodoComponent
      }
    ]),
    MatDialogModule,
    ReactiveFormsModule
  ]
})
export class TodoModule { }
