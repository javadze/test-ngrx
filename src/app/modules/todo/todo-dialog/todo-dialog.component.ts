import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import {ITodo} from '../todo.model';
@Component({
  selector: 'app-todo-dialog',
  templateUrl: './todo-dialog.component.html',
  styleUrls: ['./todo-dialog.component.scss']
})
export class TodoDialogComponent implements OnInit {
  todoForm = new FormGroup({
    text: new FormControl(''),
    description: new FormControl(''),
    isDone: new FormControl(false)
  })
  constructor(
    public dialogRef: MatDialogRef<TodoDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: ITodo) {
      if (data) {
        const form = this.todoForm.value
        Object.keys(form).forEach(key => {
          form[key] = data[key]
        })
        this.todoForm.setValue(form)
      }
    }

  ngOnInit(): void {
  }

  closeDialog() {
    this.dialogRef.close(this.todoForm.value);
  }


}
